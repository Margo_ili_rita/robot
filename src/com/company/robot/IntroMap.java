package com.company.robot;

public class IntroMap {
    private int[][] matr = new int[5][5];

    public IntroMap(){
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr[i].length; j++) {
                matr[i][j] = 10;
            }
        }
    }

    private void render(){
        int m = matr[0].length;
        int n = matr.length;
        int[][] arr = matr.clone();
        matr = new int[m+1][n+1];
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr[i].length; j++) {
                matr[i][j] = 10;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                matr[i][j] = arr[i][j];
            }
        }
        matr[0][0] = 0;
    }
    public boolean isExist(int i, int j){
        try{
            if(matr[i][j] > 10) return true;
            else{
                return false;
            }
        }
        catch (Exception e){
            render();
            return false;
        }
    }

    public int getInf(int n, int m){
        return matr[n][m];
    }

    public int getInf(Item it){
        try {
            return matr[it.getN()][it.getM()];
        }
        catch (ArrayIndexOutOfBoundsException e){
            return -1;
        }
    }

    public Item getItem(int n, int m){
        return new Item(n,m);
    }

    public void setItem(int n, int m, int value){
        isExist(n,m);
        matr[n][m] = value;
    }

    public void setItem(Item item, int value){
        try {
           // isExist(item.getN(), item.getM());
            matr[item.getN()][item.getM()] = value;
        }
        catch (Exception e){
            return;
        }

    }
    public void print(){
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr.length; j++) {
                System.out.print(matr[i][j] + " ");
            }
            System.out.println();
        }
    }

}
