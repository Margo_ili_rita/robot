package com.company.robot;

import com.company.Map;

public class Sensors implements com.company.Sensors {
    private Map map = new Map(5, 5, 2, 8);

    public Sensors(int n, int m, int countOfMuds, int countOfObstacles) {
        this.map = new Map(n, m, countOfMuds, countOfObstacles);
    }

    @Override
    public boolean isBoard(Item a) {
        try {
            return  map.getCell(a.getN(), a.getM()).equals("Г");
        }
        catch (Exception e){
            return true;
        }
    }

    @Override
    public boolean isBoard(int i, int j) {
        try {
            return  map.getCell(i, j).equals("Г");
        }
        catch (Exception e){
            return true;
        }
    }

    @Override
    public boolean isTrash(Item a) {
        if(map.getCell(a.getN(), a.getM()).equals("x")){
            map.setCell(a.getN(), a.getM(), "-");
            return true;
        }
        return false;
    }

    @Override
    public boolean isHome(Item a) {
        return (a.getM() == 0)&&(a.getN() == 0);
    }

    public void reprint(Item a){
        map.repaintRobot(a.getN(), a.getM());
    }
}
