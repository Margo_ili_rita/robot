package com.company.robot;


import com.company.Robot;
import com.company.Sensors;

/**
 * Created by Маргарита on 28.11.2017.
 */
public class Analyzer {
    private Item current, previous, next;
    private IntroMap map = new IntroMap();
    private Item e = new Item(), w = new Item(), n = new Item(), s = new Item();
    private Sensors sensors;
    private Robot robot;

    public int start(){
        do{
            analyzeItem();
            sensors.reprint(current);
        }
        while (!sensors.isHome(current));
        return robot.getScore();
    }

    private void setCurrent(Item current) {
        this.current = current;
        e = new Item(current.getN(), current.getM()+1);
        w = new Item(current.getN(), current.getM()-1);
        s = new Item(current.getN()+1, current.getM());
        n = new Item(current.getN()-1, current.getM());

    }

    public void analyzeItem(){
        if (sensors.isTrash(current)) robot.getTrash();
        map.setItem(current, 1);
        analyzeAround();
        next = findNextItem();
        if (next.equals(previous)){
            map.setItem(current, -1);
        }
        previous = current;
        setCurrent(next);
        makeStep();

    }

    private void setValue(Item a){
        if(!map.isExist(a.getN(), a.getM())){
            if(sensors.isBoard(a)){
                map.setItem(a, -2);
            }
            else {
                if(map.getInf(a) == 10){
                    map.setItem(a, 2);
                }
            }
        }
        robot.turnLeft();
    }

    private void analyzeAround(){
        for (int i = 0; i < 4; i++) {
            switch (robot.currentLook()) {
                case WEST:
                    setValue(w);
                    break;
                case EAST:
                    setValue(e);
                    robot.turnLeft();
                    break;
                case NORTH:
                    setValue(e);
                    robot.turnLeft();
                    break;
                case SOUTH:
                    setValue(s);
                    robot.turnLeft();
                    break;
            }
        }
    }

    private Item findNextItem(){
        int e, w, s, n;
        e = map.getInf(this.e);
        w = map.getInf(this.w);
        s = map.getInf(this.s);
        n = map.getInf(this.n);
        if(e>=w && e>=s && e>=n ){
            return this.e;
        }
        if(w>=e && w>=s && w>=n ) return this.w;
        if(s>=e && s>=w && s>=n) return this.s;
        return this.n;
    }

    private void makeStep(){
       // map.print();
        switch (robot.currentLook()) {
            case WEST:
                if(next.equals(w)) robot.go();
                if(next.equals(n)) {
                    robot.turnRight();
                    robot.go();
                }
                if(next.equals(s)){
                    robot.turnLeft();
                    robot.go();
                }
                if (next.equals(e)){
                    robot.turnLeft();
                    robot.turnLeft();
                    robot.go();
                }
                break;
            case EAST:
                if(next.equals(e)) robot.go();
                if(next.equals(s)) {
                    robot.turnRight();
                    robot.go();
                }
                if(next.equals(n)){
                    robot.turnLeft();
                    robot.go();
                }
                if (next.equals(w)){
                    robot.turnLeft();
                    robot.turnLeft();
                    robot.go();
                }
                break;
            case NORTH:
                if(next.equals(n)) robot.go();
                if(next.equals(e)) {
                    robot.turnRight();
                    robot.go();
                }
                if(next.equals(w)){
                    robot.turnLeft();
                    robot.go();
                }
                if (next.equals(s)){
                    robot.turnLeft();
                    robot.turnLeft();
                    robot.go();
                }
                break;
            case SOUTH:
                if(next.equals(s)) robot.go();
                if(next.equals(w)) {
                    robot.turnRight();
                    robot.go();
                }
                if(next.equals(e)){
                    robot.turnLeft();
                    robot.go();
                }
                if (next.equals(n)){
                    robot.turnLeft();
                    robot.turnLeft();
                    robot.go();
                }
                break;
        }
    }

    public Analyzer(Robot robot, Sensors sensors){
        this.robot = robot;
        this.sensors = sensors;
        setCurrent(new Item(0,0));
    }
}
