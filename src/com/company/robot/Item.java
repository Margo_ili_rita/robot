package com.company.robot;

/**
 * Created by Маргарита on 28.11.2017.
 */
public class Item {
    private int n, m;

    public Item(){}
    public Item(int n, int m){
        this.n = n;
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Item)) {
            return false;
        }

        Item item = (Item) o;

        if (n != item.n) {
            return false;
        }
        return m == item.m;
    }

    @Override
    public int hashCode() {
        int result = n;
        result = 31 * result + m;
        return result;
    }
}
