package com.company.robot;

public class Robot implements com.company.Robot {
    private Look look = Look.EAST;
    private int score = 0;

    public int getScore() {
        return score;
    }

    @Override
    public void go() {
    }

    @Override
    public void turnLeft() {
        switch (look) {
            case WEST:
                look = Look.NORTH;
                break;
            case EAST:
                look = Look.SOUTH;
                break;
            case NORTH:
                look = Look.EAST;
                break;
            case SOUTH:
                look = Look.WEST;
                break;
        }
        score--;
    }

    @Override
    public void turnRight() {
        switch (look) {
            case WEST:
                look = Look.SOUTH;
                break;
            case EAST:
                look = Look.NORTH;
                break;
            case NORTH:
                look = Look.WEST;
                break;
            case SOUTH:
                look = Look.EAST;
                break;
        }
        score--;
    }

    @Override
    public void getTrash() {
        score+=1000;
    }

    @Override
    public Look currentLook() {
        return look;
    }
}
