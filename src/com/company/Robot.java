package com.company;

import com.company.robot.Look;

public interface Robot {
    void go();
    void turnLeft();
    void turnRight();
    void getTrash();
    Look currentLook();
    int getScore();
}
