package com.company;

import com.company.robot.Analyzer;
import com.company.robot.Robot;

public class Main {

    public static void main(String[] args) {
        com.company.Robot robot = new Robot();
        Sensors sensors = new com.company.robot.Sensors(5,5,8,2);
        Analyzer analyzer = new Analyzer(robot,sensors);
        System.out.println(analyzer.start());

    }
}
