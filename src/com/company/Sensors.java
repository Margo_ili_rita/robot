package com.company;

import com.company.robot.Item;

public interface Sensors {
    boolean isBoard(Item a);
    boolean isBoard(int i, int j);
    boolean isTrash(Item a);
    boolean isHome(Item a);
    void reprint(Item a);
}
