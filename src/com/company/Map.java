package com.company;

import java.util.Random;

public class Map {

    private int sizeN, sizeM, countOfMuds, countOfObstacles;
    private String[][] map;

    public Map(int n, int m, int countOfMuds, int countOfObstacles){
        sizeN = n;
        sizeM = m;
        this.countOfMuds = countOfMuds;
        this.countOfObstacles = countOfObstacles;
        map = new String[n][m];
        initMap();
    }

    private void initMap(){
        for(int i=0; i < sizeN; i++){
            for(int j = 0; j < sizeM; j++)
                map[i][j] = "-";
        }

        int xx, yy;

        for(int i = 0; i < countOfObstacles; i++){
            xx = (new Random()).nextInt(sizeN);
            yy = (new Random()).nextInt(sizeM);
            if(map[xx][yy].equals("-"))
                map[xx][yy] = "x";
            else
                --i;
        }

        for(int i = 0; i < countOfMuds; i++){
            xx = (new Random()).nextInt(sizeN);
            yy = (new Random()).nextInt(sizeM);
            if(!map[xx][yy].equals("x"))
                map[xx][yy] = "Г";
            else
                --i;
        }

    }

    public void deletePrevPosition(int x, int y){
        map[x][y] = "0";
    }
    public void repaintRobot(int x, int y){
        paint(x,y);
    }

    public String getCell(int x, int y){
        return map[x][y];
    }
    public void setCell(int x, int y, String element){
        map[x][y] = element;
    }

    public void paint(int x, int y){
        System.out.println("------------");
        for(int i=0; i < sizeN; i++){
            for(int j=0; j < sizeM; j++) {
                if (i == x && j == y) {
                    System.out.print("! ");
                }
                else System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

}
